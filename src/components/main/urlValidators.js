function isUrl(str) {
    const result = str.match(
      /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g
    );
    if (result == null) return false;
    else return true;
  }
  
  function checkIfUrlIsImage(url) {
    // Remove any GET params
    url = url.split("?")[0];
  
    // Ensure URL leads directly to a jpeg, jpg, or png, by ensuring it ends in
    // that extension exactly
    const parts = url.split(".");
    const extension = parts[parts.length - 1];
    const imageTypes = ["jpg", "jpeg", "png"];
  
    if (imageTypes.indexOf(extension) !== -1) {
      return true;
    } else {
      return false;
    }
  }
  
  module.exports = { isUrl, checkIfUrlIsImage };