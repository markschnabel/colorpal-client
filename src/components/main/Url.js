import React, { Component } from "react";
import styled from "styled-components";

import Button from "../shared/Button";
import HelperText from "../shared/HelperText";
import { isUrl, checkIfUrlIsImage } from "./urlValidators";

const StyledInput = styled.input`
  width: 380px;
  padding: 5px 15px;
  height: 2.75rem;
  font-size: 1.15rem;
  font-weight: 200;
  margin-top: 25px;
  margin: auto;
  margin-right: 20px;
  color: #ccc;
  background: rgba(13, 24, 39, 0.5);
  outline: none;
  border-radius: 5px;
  border: solid 2px #555;
  transition: all 0.5s ease;

  &:hover {
    border-color: ${props => props.theme.accent};
  }
  &:focus {
    background: rgba(23, 34, 49, 0.75);
    border-color: #b7dfc9;
  }
  @media only screen and (max-width: 600px) {
    display: block;
    width: 380px;
    margin: 35px auto 10px auto;
  }
`;
const ResponsiveButton = styled(Button)`
  transition: all 0.5s ease;

  @media only screen and (max-width: 600px) {
    display: block;
    width: 400px;
    margin: auto;
  }
`;

class Url extends Component {
  constructor(props) {
    super(props);

    this.state = { url: "" };
  }

  handleChange = e => {
    this.setState({ url: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();

    const { url } = this.state;

    if (url === null || url.length < 1) {
      this.props.setError("Please enter a URL and try again.");
      return;
    }
    if (!isUrl(url)) {
      this.props.setError(
        "We're sorry, it seems the URL you've entered is invalid."
      );
      return;
    }
    if (!checkIfUrlIsImage(url)) {
      this.props.setError(
        "Please make sure your URL ends in one of the following: .jpeg, .jpg or .png."
      );
      return;
    }

    this.props.fetchResults({url}, url, "url");
  };

  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <form onSubmit={e => this.handleSubmit(e)}>
          <StyledInput
            value={this.state.url}
            onChange={this.handleChange}
            placeholder="Enter a URL to an image"
          />
          <ResponsiveButton type="submit">Submit</ResponsiveButton>
        </form>
        <HelperText>
          URLs must lead directly to an image and end in one of the following
          extensions: <strong>.jpg</strong>, <strong>.jpeg</strong> or
          <strong>.png</strong>
        </HelperText>
      </div>
    );
  }
}

export default Url;
