import React, { Component, Fragment } from "react";
import Dropzone from "react-dropzone";
import styled from "styled-components";

import HelperText from "../shared/HelperText";
import Button from "../shared/Button";

const UploadButton = styled(Button)`
  width: 400px;
  display: block;
  margin: auto;
  margin-bottom: 20px;
`;

const DragAndDropContainer = styled.div`
  box-shadow: ${props => (props.isDragActive ? "0 0 10px #fff" : null)};
  padding: 35px 0px;
  max-width: 800px;
  width: 100%;
  border-radius: 15px;
  border: 2px dashed ${props => (props.isDragActive ? "#fff" : "#999")};
  background: ${props =>
    props.isDragActive ? "rgba(200, 200, 200, 0.1)" : "transparent"};
  transition: all 0.25s ease;
  margin-top: 25px;
`;

const Hint = styled.p`
  font-size: 1.25rem;
  text-align: center;
  margin: 5px;

  @media only screen and (max-width: 600px) {
    display: none;
  }
`;

const dropzoneRef = React.createRef();
const ACCEPTED_FILE_TYPES = "image/jpeg, image/png";
const MAX_FILE_SIZE = 5000000; // 5 mb

class Input extends Component {
  // Function used to surpress react-dropzone's default behavior of opening a
  //  file browser any time you click within the dropzone
  handleClick = e => {
    e.preventDefault();
  };

  // Function to handle uploads
  handleDrop = (files, rejectedFiles) => {
    if (files.length + rejectedFiles.length > 1) {
      this.props.setError("Only one file can be processed at a time.");
      return;
    }
    if (rejectedFiles.length >= 1) {
      if (rejectedFiles[0].size > MAX_FILE_SIZE) {
        this.props.setError("Files must not exceed 5MB.");
        return;
      } else if (
        !ACCEPTED_FILE_TYPES.split(",").includes(rejectedFiles[0].type)
      ) {
        this.props.setError(
          "Files must end in one of the following extensions: .jpg, .jpeg, or .png."
        );
        return;
      }
    }

    const formData = new FormData();
    formData.append("image", files[0]);

    this.props.fetchResults(formData, files[0], 'upload');
  };

  render() {
    return (
      <Fragment>
        <Dropzone
          multiple={false}
          accept={ACCEPTED_FILE_TYPES}
          maxSize={MAX_FILE_SIZE}
          onDrop={this.handleDrop}
          onClick={this.handleClick}
          ref={dropzoneRef}
        >
          {({ getRootProps, getInputProps, isDragActive }) => {
            return (
              <DragAndDropContainer
                isDragActive={isDragActive}
                {...getRootProps()}
              >
                <UploadButton onClick={() => dropzoneRef.current.open()}>
                  Upload
                </UploadButton>
                <HelperText>
                  Images must be less than 5MB and use one of the following
                  extensions: <strong>.jpg</strong>, <strong>.jpeg</strong> or
                  <strong>.png</strong>
                </HelperText>
                <input {...getInputProps()} />
                <Hint>
                  <i>Hint: Drag and drop an image right here to upload it!</i>
                </Hint>
              </DragAndDropContainer>
            );
          }}
        </Dropzone>
      </Fragment>
    );
  }
}

export default Input;
