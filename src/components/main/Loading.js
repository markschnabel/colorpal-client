import React from "react";
import { PongSpinner } from "react-spinners-kit";
import styled from "styled-components";

const LoadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;
const Status = styled.h1`
  font-weight: 400;
  margin-top: 50px;
  margin-bottom: 5px;
`;

const Note = styled.p`
  font-style: italic;
  font-size: 1.2rem;
  margin: 3px 0;
  color: #ccc;
`;

const Loading = () => {
  return (
    <LoadingContainer>
      <PongSpinner size={300} color="#fff" />
      <Status style={{ fontWeight: 400, marginTop: 50 }}>
        Processing your image
      </Status>
      <Note>Note: Larger images may take longer to process!</Note>
    </LoadingContainer>
  );
};

export default Loading;
