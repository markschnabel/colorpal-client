import React, { Component } from "react";
import styled from "styled-components";
import axios from "axios";
import { isNullOrUndefined } from "util";

import FlexCenter from "../shared/FlexCenter";
import Header from "./Header";
import Url from "./Url";
import Divider from "./Divider";
import Upload from "./Upload";
import Loading from "./Loading";

const Error = styled.div`
  display: block;
  border: solid 2px tomato;
  border-radius: 5px;
  max-width: 600px;
  background: #e6c0bc;
  padding: 15px 20px;
  width: 100%;
  text-align: center;
  overflow-wrap: break-word;
  font-weight: 600;
  margin: auto;
  margin-bottom: 20px;
  margin-top: 20px;
  color: #7d2224;
  transition: all 0.75s ease;
  opacity: ${props => (props.show ? 1 : 0)};
`;

const Wrapper = styled.div`
  transition: all 0.5s ease;
  transform: ${props =>
    props.translate ? "translateY(0px)" : "translateY(-50px)"};
`;

const HideMobile = styled.div`
  text-align: center;
  @media screen and (max-width: 450px) {
    display: none;
  }
`;
const ShowMobile = styled.div`
  @media screen and (min-width: 450px) {
    display: none;
  }
`;

const PALETTE_API =
  process.env.REACT_APP_PALETTE_API_URL ||
  "http://localhost:5000/api/get_palettes/";

export class Main extends Component {
  constructor(props) {
    super(props);

    this.state = { loading: false, error: null };
  }

  setError = error => {
    this.setState({
      loading: false,
      error
    });
  };

  fetchResults = (data, imageOrUrl, inputType) => {
    this.setState({
      imageOrUrl,
      loading: true,
      error: null
    });

    const headers = {};

    if (inputType === "url") {
      headers["Content-Type"] = "application/json";
    }
    if (inputType === "upload") {
      headers["Content-Type"] = "application/json";
    }


    axios({
      method: "POST",
      url: PALETTE_API + `${inputType}`,
      data,
      headers
    })
      .then(res => {
        this.setState({ loading: false }, () => {
          this.props.history.push({
            pathname: "/results",
            state: { results: res.data, image: imageOrUrl }
          });
        });
      })
      .catch(err => {
        if (
          !isNullOrUndefined(err.response) &&
          !isNullOrUndefined(err.response.data) &&
          !isNullOrUndefined(err.response.data.error)
        ) {
          this.setState({ error: err.response.data.error, loading: false });
        } else {
          console.log(err);
          this.setState({
            error:
              "We're sorry. An unknown error has occured. Please try again shortly",
            loading: false
          });
        }
      });
  };

  render() {
    const { loading, error } = this.state;

    if (loading) {
      return <Loading />;
    }

    return (
      <FlexCenter
        style={{ marginBottom: "100px", marginTop: "25px" }}
        direction="column"
      >
        <HideMobile>
          <Header />
          <Error show={error ? true : false}>
            <p style={{ margin: 0 }}>{error}</p>
          </Error>
          <Wrapper translate={error ? true : false}>
            <Url setError={this.setError} fetchResults={this.fetchResults} />
            <Divider />
            <Upload setError={this.setError} fetchResults={this.fetchResults} />
            <p style={{ textAlign: "center" }}>
              Note: this app is currently under development.
            </p>
          </Wrapper>
        </HideMobile>
        <ShowMobile>
          <h1 style={{ textAlign: "center", marginTop: "100px" }}>
            Mobile Support Coming Soon!
          </h1>
        </ShowMobile>
      </FlexCenter>
    );
  }
}

export default Main;
