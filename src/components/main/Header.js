import React, { Fragment } from "react";
import styled from "styled-components";
import { FaImage } from "react-icons/fa";

import FlexCenter from "../shared/FlexCenter";

const StyledHeader = styled.h1`
  font-size: 4rem;
  margin: 10px 15px;
  @media only screen and (max-width: 600px) {
    font-size: 3rem;
  }
`;
const StyledIcon = styled(FaImage)`
  margin-top: 20px;

  font-size: 4rem;
  @media only screen and (max-width: 600px) {
    font-size: 3rem;
  }
`;
const SubHeader = styled.h3`
  font-size: 1.15rem;
  margin: 5px;
  font-weight: 200;
  max-width: 800px;
  text-align: center;
  @media only screen and (max-width: 600px) {
    font-size: 1rem;
  }
`;

const Header = () => {
  return (
    <Fragment>
      <FlexCenter direction={"row"}>
        <StyledHeader>ColorPal</StyledHeader>
        <StyledIcon />
      </FlexCenter>
      <SubHeader>
        ColorPal is a tool that allows you to create perfect color palettes
        based off of any image. Just choose one of the methods below to submit
        an image, and we'll take care of the rest.
      </SubHeader>
    </Fragment>
  );
};

export default Header;
