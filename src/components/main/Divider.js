import React from "react";
import styled from "styled-components";

import FlexCenter from '../shared/FlexCenter';

const Text = styled.h3`
  font-size: 2rem;
  font-weight: 400;
  letter-spacing: 1px;
  display: inline;
  margin: 10px 15px;
`;
const Line = styled.div`
  width: 60px;
  height: 2px;
  background: white;
  display: inline-block;
`;

const Divider = () => {
  return (
    <FlexCenter>
      <Line />
      <Text>OR</Text>
      <Line />
    </FlexCenter>
  );
};

export default Divider;
