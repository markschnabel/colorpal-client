import React, { Component } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import styled from "styled-components";

const Swatch = styled.div`
  height: 70px;
  width: 70px;
  background: ${props => props.background};
  border-radius: 5px;
  border: solid 1px white;
  margin: auto;
`;
const RGB = styled.p`
  font-size: 1.2em;
  margin: 5px 0 -5px 0;
  text-align: center;
  font-weight: 600;
`;
const ColorValue = styled.p`
  font-size: 1.2em;
  margin: 5px 0;
  font-weight: 400;
  text-align: center;
  color: #ccc;
  transition: all 0.5s ease;
  &:hover {
    cursor: pointer;
    color: lightblue;
  }
`;
const Color = styled.div`
  display: inline-block;
  text-align: center;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 130px;
  padding: 10px 0;
  margin: auto;

  &:hover {
    box-shadow: 0px 9px 39px -10px rgba(0, 0, 0, 0.59);
    background: rgba(200, 200, 200, 0.2);
  }
`;
const Palette = styled.div`
  text-align: center;
`;
const Header = styled.h2`
  font-size: 2rem;
  text-align: center;
  margin: 6px 0;
`;
const Description = styled.p`
  display: block;
  text-align: center;
  margin: 5px 15px 25px 15px;
  font-weight: 400;
  font-size: 1rem;
  color: #bbb;
`;

const handleCopy = (text, result) => {
  result
    ? alert(`Copied "${text}" to clipboard!`)
    : alert(`Oops! Something wen't wrong. Please try again.`);
};

function unpackColorPalette(colorPalette) {
  return colorPalette.map((data, index) => {
    return (
      <Color
        key={data.hex + "-" + index}
        onClick={() => document.execCommand("copy")}
      >
        <Swatch background={data.hex} />
        <RGB>R,&nbsp;&nbsp;G,&nbsp;&nbsp;B</RGB>
        <CopyToClipboard
          text={data.rgb.join(", ")}
          onCopy={(text, result) => handleCopy(text, result)}
        >
          <ColorValue copyId={"copy-" + index}>
            {data.rgb.join(", ")}
          </ColorValue>
        </CopyToClipboard>
        <CopyToClipboard
          text={data.hex}
          onCopy={(text, result) => handleCopy(text, result)}
        >
          <ColorValue copyId={"copy-" + index}>{data.hex}</ColorValue>
        </CopyToClipboard>
      </Color>
    );
  });
}

export class ColorPalette extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mouseIsOverColor: false
    };
  }

  render() {
    const { data, header, description } = this.props;

    if (data.hasOwnProperty("error")) {
      return (
        <div style={{ width: "100%", textAlign: "center", margin: "auto" }}>
          <Header>We're sorry</Header>
          <Description>
            It appears something went wrong when contacting the machine learning
            service we utilize. The service may just be down for maintenance.
            Please try again shortly.
          </Description>
        </div>
      );
    }
    return (
      <div style={{ width: "100%" }}>
        <Header>{header}</Header>
        <Description>{description}</Description>
        <Palette>{unpackColorPalette(data)}</Palette>
      </div>
    );
  }
}

export default ColorPalette;
