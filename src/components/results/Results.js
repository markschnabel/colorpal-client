import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { Fade } from "react-reveal";
import styled from "styled-components";
import { Grid, Row, Col } from "react-flexbox-grid";

import ColorPalette from "./ColorPalette";

const UsersImage = styled.img`
  max-height: 450px;
  max-width: 450px;
  width: 100%;
  height: 100%;
  min-height: 250px;
  min-width: 250px;
  display: block;
  margin: auto;
  border-radius: 15px;
  border: solid 2px white;
  object-fit: cover;
`;

export default class Results extends Component {
  constructor(props) {
    super(props);

    this.state = {
      imageSrc: null
    };
  }

  componentWillMount() {
    this.loadImageIntoState();
  }

  loadImageIntoState = () => {
    this.image = this.props.location.state.image;
    this.results = this.props.location.state.results;

    const fileReader = new FileReader();

    fileReader.onload = e => {
      this.setState({ imageSrc: e.target.result });
    };

    fileReader.readAsDataURL(this.image);
  };

  render() {
    if (
      this.props.location.state === null ||
      this.props.location.state === undefined
    ) {
      return <Redirect to="/" />;
    }
    if (this.image === null || this.image === undefined) {
      return <Redirect to="/" />;
    }
    if (this.results === null || this.results === undefined) {
      return <Redirect to="/" />;
    }

    return (
      <Grid style={{ marginTop: "100px" }}>
        <Row middle="lg">
          <Col lg={4}>
            <Fade bottom cascade>
              <div>
                <UsersImage src={this.state.imageSrc} alt="Your Image" />
              </div>
            </Fade>
          </Col>
          <Col lg={8}>
            <Fade bottom distance="200px" delay={250}>
              <ColorPalette
                data={this.results.base_color_palette}
                header="Your Image's Color Palette"
                description="Here you can see a palette consisting of the most highly concentrated colors in your image."
              />
              <ColorPalette
                data={this.results.enhanced_color_palette}
                header="Machine Learning Enhanced Palette"
                description={`A palette we've gathered for you by feeding the palette 
                above into a machine learning model that has been trained on thousands 
                of palettes that utilize the best aspects of color theory.`}
              />
            </Fade>
          </Col>
        </Row>
      </Grid>
    );
  }
}
