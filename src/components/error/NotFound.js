import React, { Fragment } from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

const StatusCode = styled.h1`
  font-size: 8vw;
  margin: 0;
  color: white;
`;
const Info = styled.p`
  font-weight: 500;
  font-size: 1.25rem;
`;
const StyledList = styled.ul`
  margin: 2px;
`;
const StyledLink = styled(NavLink)`
  font-size: 1.25rem;
  font-weight: 400;
  display: block;
  margin: 20px 0;
  color: lightblue;
  text-decoration: none;
  transition: all 0.5s ease;

  &:hover {
    color: lightblue;
  }
`;

const NotFound = () => {
  return (
    <Fragment>
      <StatusCode>404</StatusCode>
      <Info>
        We're sorry. It seems the page you were looking for could not be
        located.
      </Info>
      <Info>Choose any of the following pages to return:</Info>
      <StyledList>
        <li>
          <StyledLink to="/">Home</StyledLink>
        </li>
        <li>
          <StyledLink to="/about">About</StyledLink>
        </li>
      </StyledList>
    </Fragment>
  );
};

export default NotFound;
