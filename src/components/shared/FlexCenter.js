import styled from "styled-components";

const FlexCenter = styled.div`
  display: flex;
  flex-direction: ${props => props.direction};
  align-items: center;
  justify-content: center;
`;

export default FlexCenter;