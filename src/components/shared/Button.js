import styled from "styled-components";

const Button = styled.button`
  color: white;
  background: linear-gradient(45deg, #1a7684 30%, #50a08e 90%);
  font-size: 1.25rem;
  font-weight: 500;
  font-family: Arial, Helvetica, sans-serif;
  border-radius: 5px;
  border: none;
  cursor: pointer;
  padding: 15px 25px;
  box-shadow: 0 10px 10px 2px rgba(5, 5, 5, 0.2);
  transition: all 0.5s ease;

  &:hover {
    transform: translateY(-5px);
    box-shadow: 0 17.5px 17.5px 2px rgba(5, 5, 5, 0.4);
  }
`;

export default Button;
