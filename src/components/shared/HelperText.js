import styled from "styled-components";

const HelperText = styled.p`
  color: #ddd;
  margin-top: 10px;
  margin-left: 5px;
  font-size: 1rem;
  text-align: center;
  overflow-wrap: break-word;
`;

export default HelperText
