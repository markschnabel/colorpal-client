import React from "react";
import styled from "styled-components";

import Navbar from "./Navbar";
import Footer from "./Footer";
import Container from "../shared/Container";

const Content = styled.div`
  min-height: 100vh;
  height: 100%;
  padding-bottom: 60px;
`;

const CoreContentContainer = styled.div`
  background: radial-gradient(
    circle,
    rgba(0, 27, 68, 0.25) 50%,
    rgba(7, 12, 27, 1) 100%
  );
`;

const Layout = ({ children }) => {
  return (
    <div>
      <CoreContentContainer>
        <Navbar />
        <Container>
          <Content>{children}</Content>
        </Container>
      </CoreContentContainer>
      <Footer />
    </div>
  );
};

export default Layout;
