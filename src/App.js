import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Layout from "./components/layout/Layout";
import Main from "./components/main/Main";
import About from "./components/about/About";
import Results from "./components/results/Results";
import NotFound from "./components/error/NotFound";

class App extends Component {
  render() {
    return (
      <Router>
        <Layout>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/about" component={About} />
            <Route exact path="/results" component={Results} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Layout>
      </Router>
    );
  }
}

export default App;
